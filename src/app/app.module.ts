import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';

import { MdButtonModule, MdInputModule, MdToolbarModule } from '@angular/material';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from "@angular/router";
import { MyWorksComponent } from "../app/my-works/my-works.component";
import { BlogComponent } from './blog/blog.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { HomeComponent } from './home/home.component';
import { TitlebarComponent } from './titlebar/titlebar.component';

@NgModule({
  declarations: [
    AppComponent,
    BlogComponent,
    AboutMeComponent,
    HomeComponent,
    MyWorksComponent,
    TitlebarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdInputModule,
    MdToolbarModule,
    RouterModule.forRoot([
      { path:'myWorks', component: MyWorksComponent },
      { path:'blog', component: BlogComponent },
      { path:'aboutMe', component: AboutMeComponent },
      { path:'home', component: HomeComponent }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
